@flat-file-validation
@issue:TAP-1
Feature: Flat file validation

  Background:
    Given a directory "../sample_data/output" of flat files
    When the number of files equal to 3

  @issue:TAP-2
  Scenario: Flat file name check
  As a developer,
  I want to know if the name of the converted files are correct,
  so that I can ensure that my dependent components will work

    Then the file name should match the pattern "^[a-zA-Z0-9\-]{11}\_dne\.tst$"

  @issue:TAP-3
  Scenario: Record structure check
  As a developer,
  I want to know if the content of records are correct,
  so that I can ensure that my dependent components will work
    And the file names are verified against "^[a-zA-Z0-9\-]{11}\_dne\.tst$"
    Then lines matching with "^[A-Z]{3}\ \| .*\ #$" shall be verified

  @issue:TAP-4
  Scenario: Record count check
  As a developer,
  I want to know if the structure of the converted files are correct,
  so that I can ensure that my dependent components will work
    And the file names are verified against "^[a-zA-Z0-9\-]{11}\_dne\.tst$"
    Then the number of lines in file matching with "^[A-Z]{3}\ \| .*\ #$" are 5

  @manual
  Scenario: Flat file manual check

  @ignored
  Scenario: This test is ignored

