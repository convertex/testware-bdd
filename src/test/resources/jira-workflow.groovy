when 'to do', {
    'success' should: 'in progress'
}

when 'in progress', {
    'success' should: 'done'
}

when 'done', {
    'failure' should: 'to do'
}
