package dev.basetrap.bdd.rest;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

import java.util.Arrays;
import java.util.List;
import java.util.logging.Logger;

import static io.restassured.RestAssured.*;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.equalTo;

public class RESTAPIStepDefinition {

    Logger log = Logger.getLogger(RESTAPIStepDefinition.class.getName());

    private static final String BASE_URI_STRING = "http://localhost:3000";

    @Given("the server running")
    public void the_server_running() {
        given()
                .get(BASE_URI_STRING.toString())
                .then()
                .statusCode(200);
    }

    @When("{string} path requested")
    public void path_requested(String path) {
        when()
                .get(BASE_URI_STRING.toString() + path)
                .then()
                .statusCode(200);
    }

    @Then("a list of JSON objects should return for the path {string}")
    public void a_list_of_json_objects_should_return_for_the_path(String path) {
        final int test_data_size = 11;

        get(BASE_URI_STRING.toString() + path)
                .then()
                .assertThat()
                .body("size()", is(test_data_size));

        Response response = get(BASE_URI_STRING + path);

        List<Project> allProjects = Arrays.asList(response.as(Project[].class));
        allProjects.forEach(project -> System.out.println("PK: " + project.getProjectKey() + " | Name: " + project.getName()));
    }

    @Then("a JSON object should return for path {string} with key {string}")
    public void a_json_object_should_return_for_path_and_key_key(String path, String projectKey) {
        final int test_data_size = 1;
        get(BASE_URI_STRING.toString() + path + "/" + projectKey)
                .then()
                .assertThat()
                .body("size()", is(test_data_size))
                .and()
                .body("projectKey[0]", equalTo(projectKey));

        Response response = get(BASE_URI_STRING + path);

        JsonPath jsonPathEvaluator = response.jsonPath();
        System.out.println(jsonPathEvaluator.get("projectKey").toString());
    }
}

class Project {
    private String projectKey;
    private String name;

    public String getProjectKey() {
        return projectKey;
    }

    public String getName() {
        return name;
    }
}