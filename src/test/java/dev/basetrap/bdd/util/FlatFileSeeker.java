package dev.basetrap.bdd.util;


import java.util.List;
import java.util.logging.Logger;

public class FlatFileSeeker {

    private Logger log = Logger.getLogger(FlatFileSeeker.class.getName());

    public FlatFileSeeker() {
    }

    public String letMeSee(String flatFileDirectory, List<String> verifiedFlatFileNames, String searchKey) {

        for (String flatFileName : verifiedFlatFileNames) {
            String flatFileFullPath = flatFileDirectory + flatFileName;
            // connecting input file with output file based on input ID and output file name
            if (flatFileFullPath.contains(searchKey)) {
                return flatFileName;
            }
        }
        return "";
    }

}
