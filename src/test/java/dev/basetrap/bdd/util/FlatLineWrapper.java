package dev.basetrap.bdd.util;

import java.util.logging.Logger;

public class FlatLineWrapper {

    private Logger log = Logger.getLogger(FlatLineWrapper.class.getName());

    private String[] fields;

    public void parse(String line) {
        fields = line.substring(0, line.length() - FlatFile.END_OF_THE_ROWAD.length()).split("\\|");
    }

    public String getFiledTrimmed(int number) {
        if (!fields[number].equals(FlatFile.EMPTY_FIELD)) {
            return fields[number].trim();
        }
        return "[ FIELD IS EMPTY ]";
    }

    public int getNumberOfFields() {
        return fields.length - 1;
    }
}
