package dev.basetrap.bdd.util;

public class JzonFile {

    public static final int OBJECT_TO_PROCESS_ARRAY_INDEX = 0;

    public static final String ASSIGNEE = "Assignee";

    public static final String COUNTRY = "Country";

    public static final String DATE = "Date";

    public static final String DETAILS = "Details";

    public static final String DIVISION = "Division";

    public static final String EORI = "EORI";

    public static final String EXPORTER = "Exporter";

    public static final String ID = "ID";

    public static final String INVOICES_COUNT = "InvoicesCount";

    public static final String PAGES_COUNT = "PagesCount";

    public static final String PROCESSING_TIME = "ProcessingTime";

    public static final String STATUS = "Status";

    public static final String USER_TIME = "UserTime";



}
