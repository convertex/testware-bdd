package dev.basetrap.bdd.util;

public class FlatFile {

    public static final String END_OF_THE_ROWAD = "#";

    public static final String EMPTY_FIELD = "#";

    public static final int SECTION_NAME_POS = 0;

    public enum SECTION_NAMES {
        STR,
        GEN,
        TEC,
        DET,
        END
    }

    public enum STR_FIELDS {
        STR_SECTION_ID_POS,
        STR_SECTION_DATE_POS
    }

    public enum GEN_FIELDS {
        GEN_SECTION_COUNTRY_POS,
        GEN_SECTION_DIVISION_POS,
        GEN_SECTION_STATUS_POS
    }

    public enum TEC_FIELDS {
        TEC_SECTION_DOCUMENTS_POS,
        TEC_SECTION_NUMBER_OF_PAGES_POS,
        TEC_SECTION_ERR_POS
    }

    public enum DET_FIELDS {
        DET_SECTION_CLERK_POS,
        DET_SECTION_TRADER_POS,
        DET_SECTION_INPUT_TIME_POS,
        DET_SECTION_MESSAGE_POS
    }

    public enum END_FIELDS {
        END_SECTION_END_TIME_POS
    }
}
