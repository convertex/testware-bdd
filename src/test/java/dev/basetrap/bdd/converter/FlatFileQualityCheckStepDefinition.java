package dev.basetrap.bdd.converter;

import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.core.Serenity;
import net.thucydides.core.annotations.Issue;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

@Issue("TAP-1")
public class FlatFileQualityCheckStepDefinition {

    private Logger log = Logger.getLogger(FlatFileQualityCheckStepDefinition.class.getName());

    private List<String> flatFileNames;

    private List<String> verifiedFlatFileNames;

    private String flatFileDirectory;

    @Before
    public void setUp() {
        flatFileNames = new ArrayList<>();
        verifiedFlatFileNames = new ArrayList<>();
    }

    @Given("a directory {string} of flat files")
    public void aaaaa(String string1) {

        String basePath = new File("").getAbsolutePath();
        flatFileDirectory = new File(basePath + File.separator + string1).getAbsolutePath();

        flatFileNames = Stream.of(new File(flatFileDirectory).listFiles())
                .filter(file -> !file.isDirectory())
                .map(File::getName)
                .collect(Collectors.toList());
    }

    @When("the number of files equal to {int}")
    public void bbbbb(int int1) {
        assertThat(int1, equalTo(flatFileNames.size()));
    }

    @Then("the file name should match the pattern {string}")
    public void ccccc(String string1) {
        for (String fileName : flatFileNames) {
            Pattern pattern = Pattern.compile(string1);
            Matcher matcher = pattern.matcher(fileName);
            if (!matcher.matches()) {
                Serenity.recordReportData().withTitle("File naming check error").andContents(fileName);
            }
            assertThat(matcher.matches(), equalTo(Boolean.TRUE));
        }
    }

    @And("the file names are verified against {string}")
    public void theFileNamesAreVerified(String string1) {
        for (String fileName : flatFileNames) {
            Pattern pattern = Pattern.compile(string1);
            Matcher matcher = pattern.matcher(fileName);
            if (matcher.matches()) {
                verifiedFlatFileNames.add(fileName);
            }
        }
    }

    @And("the number of lines in file matching with {string} are {int}")
    public void theNumberOfLinesInFileAre(String string1, int int1) {

        for (String flatFileName : verifiedFlatFileNames) {
            String fullPath = flatFileDirectory + File.separator + flatFileName;
            List<String> allLines;
            try {
                allLines = Files.readAllLines(Paths.get(fullPath));
            } catch (IOException e) {
                throw new RuntimeException(e);
            }

            AtomicInteger count = new AtomicInteger();
            for (String line : allLines) {
                Pattern pattern = Pattern.compile(string1);
                Matcher matcher = pattern.matcher(line);
                if (matcher.matches()) {
                    count.getAndIncrement();
                }
            }
            assertThat(count.get(), equalTo(int1));
        }
    }

    @Then("lines matching with {string} shall be verified")
    public void recordsShallBeValidated(String string1) {
        for (String flatFileName : verifiedFlatFileNames) {
            String fullPath = flatFileDirectory + File.separator + flatFileName;

            List<String> allFlatLines;
            try {
                allFlatLines = Files.readAllLines(Paths.get(fullPath));
            } catch (IOException e) {
                throw new RuntimeException(e);
            }

            for (String line : allFlatLines) {
                Pattern pattern = Pattern.compile(string1);
                Matcher matcher = pattern.matcher(line);

                assertThat(matcher.matches(), equalTo(Boolean.TRUE));
            }

        }
    }
}
